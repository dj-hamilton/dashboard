<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Open Retail Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<?php
session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/config.php');
//If User has already logged in, redirect to dashboard page.
if (isset($_SESSION['user_logged_in']) && $_SESSION['user_logged_in'] === TRUE) {
header('Location:index.php');
}

//If user has previously selected "remember me option", his credentials are stored in cookies.
if(isset($_COOKIE['username']) && isset($_COOKIE['password']))
{
//Get user credentials from cookies.
$username = filter_var($_COOKIE['username']);
$passwd = filter_var($_COOKIE['password']);
$db = getDbInstance();
$db->where ("user_name", $username);
$db->where ("passwd", $passwd);
$row = $db->get('admin_accounts');

if ($db->count >= 1)
{
//Allow user to login.
$_SESSION['user_logged_in'] = TRUE;
$_SESSION['admin_type'] = $row[0]['admin_type'];
header('Location:index.php');
exit;
}
else //Username Or password might be changed. Unset cookie
{
unset($_COOKIE['username']);
unset($_COOKIE['password']);
setcookie('username', null, -1, '/');
setcookie('password', null, -1, '/');
header('Location:login.php');
exit;
}
}



?>
<div id="page-" class="col-md-4 col-md-offset-4">
    <form class="form loginform" method="POST" action="authenticate.php">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
            <img src='../images/logo.png' style='display: block; margin-left: auto; margin-right: auto;'>
                <h3 class="panel-title">Please Sign In</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label">Username</label>
                    <input type="text" placeholder="Username" name="username" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label class="control-label">Password</label>
                    <input type="password" placeholder="Password" name="passwd" class="form-control" required="required">
                </div>
                <div class="checkbox">
                    <label>
                        <input name="remember" type="checkbox" value="1">Remember Me
                    </label>
                </div>
                <?php
                if(isset($_SESSION['login_failure'])){ ?>
                    <div class="alert alert-danger alert-dismissable fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $_SESSION['login_failure']; unset($_SESSION['login_failure']);?>
                    </div>
                <?php } ?>
                <button type="submit" class="btn btn-lg btn-success btn-block" >Login</button>
            </div>
        </div>
    </form>
</div>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
