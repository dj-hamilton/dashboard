###Dashboard 2018/2019 
#####List of Functions and Features .

##### Master Features
- Online Secure Login (Two Factor??//??)
- Reset Password
- Automatic Offsite backup of Database and File
- Modular development - To avoid cluttered directory from Main Dashboard and features.

######Client Features 
- REST API for Addding and Updating Sales and Stock Records offisite
- Full-featured: Real-time online Sales and Stock Reporter 
    -Line by line Sales Reports
    -Line by line Stock Reports 

- System to submit requests / changes / suggestions 
- Full error handling and notification for Employers of Open Retail Solutions 

######Open Retail Solutions Features 
- Client / Customer List 
    -Contact Details, Password, Notes
    -Add / Remove / Update feature

- Ticketing System / Notes. Jobs that can be raised and logged against each customer

- Master User to view each of the Databases for sales / stock information. Clients to be made aware and Opt in / out 


## Current Features Added

- Ability to login and maintain users (Need to add reset function)
- REST API for adding, updating and delelting records. Need to decide if to list table content for updating or to work from schematics 
- Client list with Details and Notes, No Passwords yet.


########### External Resources
- Bootstrap CSS
